<!--
SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project

SPDX-License-Identifier: CC-BY-4.0
-->

# Governance of HPCToolkit, part of the HPCToolkit Project

This repository is part of the HPCToolkit Project, a series of LF Projects, LLC. Information on the governance of the HPCToolkit Project is available at https://gitlab.com/hpctoolkit/governance.

## Project Leads

The current Project Leads for this repository are as follows:

- [John Mellor-Crummey](https://gitlab.com/jmellorcrummey)
- [Mark Krentel](https://gitlab.com/mwkrentel)
- [Laksono Adhianto](https://gitlab.com/adhianto)
- [Jonathon Anderson](https://gitlab.com/blue42u)
