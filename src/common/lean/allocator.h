// SPDX-FileCopyrightText: 2002-2024 Rice University
// SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
//
// SPDX-License-Identifier: BSD-3-Clause

// -*-Mode: C++;-*- // technically C99

//***************************************************************************
//
// File:
//   $HeadURL$
//
// Purpose:
//   interface for a dynamic allocator
//
//***************************************************************************

#ifndef __allocator_h__
#define __allocator_h__


//***************************************************************************
// type declarations
//***************************************************************************

typedef void *(allocator_t)(size_t nbytes);



//***************************************************************************


#endif /* __allocator_h__ */
