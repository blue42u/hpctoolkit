#!/usr/bin/env python3

# SPDX-FileCopyrightText: 2023-2024 Rice University
# SPDX-FileCopyrightText: 2024 Contributors to the HPCToolkit Project
#
# SPDX-License-Identifier: BSD-3-Clause

# pylint: disable=invalid-name,missing-module-docstring

import contextlib
import os
import signal
import time


def func_hi() -> None:
    "Top-of-stack function"
    func_mid()


def func_mid() -> None:
    "Middle-of-stack function"
    func_lo()


def func_lo() -> None:
    "Leaf function"
    time.sleep(0.01)
    os.kill(os.getpid(), signal.SIGINT)


for _i in range(10):
    with contextlib.suppress(KeyboardInterrupt):
        func_hi()
