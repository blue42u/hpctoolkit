<!-- Summarize the crash here. How did the CI fail in ways it shouldn't have? -->

### Failing Jobs/Pipelines

<!-- Link to the failing jobs and/or pipelines. -->

- Job [#12345](https://gitlab.com/hpctoolkit/hpctoolkit/-/jobs/12345)

### Common Errors

<!-- Copy any common error messages from the failing jobs in a code block. -->

### Root Cause

<!-- If you have investigated the issue and have some insight into the root cause, please describe any insights here. -->

<!-- Do not remove the following lines. -->

/label ~"component::dev"
/label ~"type::bug"
