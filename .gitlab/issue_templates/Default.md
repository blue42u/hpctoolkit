BEFORE YOU CONTINUE: Please read through the CONTRIBUTING.md for detailed tips and tricks for a successful issue.

And thank you for helping improve HPCToolkit!
